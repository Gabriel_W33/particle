#include "Particle.h"


void Particle::setup(double _rx, double _ry, double _rz){
	ex = _rx;
	ey = _ry;
	ez = _rz;

	dt = 0.01;
	
	r = ofRandom(180,255);
	g = ofRandom(160,190);
	b = 0;
	

	vx = ofRandom(-10,10);
	vy = ofRandom(-10,100);
	vz = ofRandom(-10,10);


 	double K1 = ofRandom(0, 360);
 	double K2 = ofRandom(0, 180);
	
	R = ofRandom(1 , 50);

	x = R * cos(K2)*cos(K1);
	y = R * cos(K2)*sin(K1);
	z = R * sin(K2);


	rx = ex + x;
	ry = ey + y;
	rz = ez + z;


	size = 1;
	setLifeTime();
	procent=timeLife;
	

}

void Particle::update() {


	FAx = -6 * 5 *vx;
	FAz = -6 * 5* vz;
	FAy = 7000 + (-6 * 5 * vy);


	Fx = FAx +  wind.ForceX;
	Fy = FAy +  wind.ForceY;
	Fz = FAz +  wind.ForceZ;

	vy = vy + (Fy * dt);
	vx = vx + (Fx * dt);
	vz = vz + (Fz * dt);


	ry = ry + (vy * dt);
	rx = rx + (vx * dt);
	rz = rz + (vz * dt);


	timeLife -= 1;
	g -= 10;
	alfa = 100 * (timeLife / procent);
}

void Particle::setLifeTime()
{
	double L = (sqrt((x*x) + (z*z)) + 0.2) / 50;
	
	timeLife = 50 * (abs(L-1));
}


void Particle::draw() {

	ofSetColor(r, g, b, alfa);
	ofDrawSphere(rx, ry, rz, size);
	
	

}


void Particle::setWind(Wind _wind)
{
	wind = _wind;
}



Particle::~Particle()
{

	Wind wind;
}
