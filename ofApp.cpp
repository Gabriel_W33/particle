#include "ofApp.h"



//--------------------------------------------------------------
void ofApp::setup()
{

	gui.setup();
	gui.add(sX.setup("windX", 0, -10000, 10000));
	gui.add(sY.setup("WindY", 0, -10000, 10000));
	gui.add(sZ.setup("WidnZ", 0, -10000, 10000));
	
	fire.load("ss.wav");
	fire.setVolume(0.5);
	fire.setLoop(true);
	fire.play();
	//ofEnableDepthTest();
	//ofSetVerticalSync(true);
	wind.setup(sX, sY, sZ);
	emiter.setup(50, 300);
	
	
}

//--------------------------------------------------------------
void ofApp::update() {
	
	wind.setup(sX, sY, sZ);
	emiter.setWind(wind);
	emiter.update();
	
	
}
//--------------------------------------------------------------
void ofApp::draw(){
	
	
	ofSetBackgroundColor(0,128,128);
	cam.begin();
	
	ofDrawAxis(1000);
	
	
	emiter.draw();

	cam.end();
	gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	int s = 5;
	switch (key)
	{
	case 'a':
		emiter.x -= s;
		break;
	case 's':
		emiter.y -= s;
		break;
	case 'd':
		emiter.x += s;
		break;
	case 'w':
		emiter.y += s;
		break;
	case 'q':
		emiter.z -= s;
		break;
	case 'e':
		emiter.z += s;

		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
