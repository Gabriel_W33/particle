#pragma once
#include "ofMain.h"
#include <list>
#include <iostream>
#include <cstdlib>
#include "Particle.h"
#include "Wind.h"


class Emiter
{

private:
	double size;
	double v;

	int r;
	int g;
	int b;

	double dt;
	void setColor(int, int, int);
	

	list <Particle*> plist;
public:
	


	void setup(double s, double v_);
	void draw();
	void update();
	
	void emit();
	void setWind(Wind wnd);

	double x;
	double y;
	double z;
	
	Wind wind;
	~Emiter();
};

