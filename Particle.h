#pragma once


#include "ofMain.h"
#include <cmath>
#include <conio.h>
#include <math.h>
#include "Wind.h"


class Particle{

private:

	double dt;
	double ex;
	double ey;
	double ez;

	double	x;
	double	y;
	double	z;
	
	double rx;
	double ry;
	double rz;

	double vx;
	double vy;
	double vz;

	double size;
	double FAy;
	double FAx;
	double FAz;

	double Fy;
	double Fx;
	double Fz;

	double WindFy;
	double WindFx;
	double WindFz;
	Wind wind;

public:

	~Particle();
	void setup(double, double, double);
	

	void draw();
	void update();
	void setLifeTime();

	double procent;
	double R;
	int timeLife;
	void setWind(Wind wnd);

	int r, g, b ,alfa;

};