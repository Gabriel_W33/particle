#include "Emiter.h"



void Emiter::setup(double s, double v_)
{
	size = s;
	v = v_;
	x = 0;
	y = 0;
	z = 0;


	dt = 0.01;

	for (int i = 0; i <= 10; i++) {
		Particle *particle = new Particle;
		particle->setup(x, y, z);
		
		plist.push_back(particle);
	} 
}


void Emiter::draw()
{
	emit();
}

void Emiter::setColor(int _r, int _g, int _b) {
	r = _r;
	g = _g;
	r = _b;
}


void Emiter::update() {


	for (list<Particle*>::iterator iter = plist.begin(); iter != plist.end();) {
		Particle* tempP = *iter;
		tempP->setWind(wind);
		tempP->update();
		
		if (tempP->timeLife <= 0) {
			iter = plist.erase(iter);
		}
		else
		{
			iter++;
		}
		
	};
	
	for (int i = 0; i <= 1000; i++) {
		Particle *particle = new Particle;
		particle->setup(x, y, z);

		plist.push_back(particle);
	};


}

void Emiter::emit() {

	for (list<Particle*>::iterator iter = plist.begin(); iter != plist.end(); iter++) {
		Particle* tempP = *iter;
		tempP->draw();

	};

}

void Emiter::setWind(Wind _wind)
{
	wind = _wind;
}






Emiter::~Emiter()
{
}
